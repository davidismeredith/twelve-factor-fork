const {Pool} = require('pg')
// Explicity uses environment variables - see docker-compose.yml. Use of docker secrets or similar is recommend to store sensitive data.
// Demonstration of stateless.
const pool = new Pool()

async function getTodoTotal() {
    const client = await pool.connect()
    try {

        const res = await client.query('SELECT * from HARTREE.TODO WHERE ID = 1')
        console.log(`The Todo total is ${res.rows[0].total}. The postgres host is ${process.env.PGHOST}.`);
    } catch (e) {
        console.error('Postgres Error Occurred', e);
        throw e;
    }
    finally
    {
        client.release()
    }
}

async function setTodoTotal() {
    const client = await pool.connect()
    try {
        await client.query('UPDATE HARTREE.TODO SET TOTAL = TOTAL + 1 WHERE ID = 1')
        await client.query('COMMIT')
    } catch (e) {
        console.error('Postgres Error Occurred', e);
        await client.query('ROLLBACK')
        throw e;
    } finally {
        client.release()
    }
}

getTodoTotal()

setTodoTotal()

setTimeout(() => process.exit(), 3000);
