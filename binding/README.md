## ## Twelve Factors - binding

In this example, we demonstrate port binding. The container's port is listening on port `5000` and is exposed on localhost via `5009`. Note that you can't connect directly to the container's port, you need to go via the mapped external port.  

```shell
docker build -t hartree/binding:1.0 .

docker run -d -p 5009:5000 hartree/binding:1.0 .
```

For more info see: https://docs.docker.com/config/containers/container-networking/

### multiple instances can be started, for example

```shell
docker run -d -p 5010:5000 hartree/binding:1.0 .
```
