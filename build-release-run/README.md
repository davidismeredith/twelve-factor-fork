## Twelve Factors - Build, release, run

docker build -t hartree/build-release-run:1.0 .

docker-compose up

### note the port is internally listening on port 8080 and mapped to 4444.

