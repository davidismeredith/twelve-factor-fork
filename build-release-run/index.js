const express = require('express');

const app = express();

app.use(express.static('public'))

app.get('/', function (req, res) {
  res.send('<h1>Hi, this is the build, release, and run example!</h1><img src="images/sunset.jpeg"  alt="a sunset over the sea" width="800" height="800"/>')
})

app.listen(8080, () => console.log('Application running on port 8080'));
