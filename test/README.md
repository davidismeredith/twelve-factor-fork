# Twelve Factors - test!

- [Getting started](#getting-started)
- 
- [References](#handy-references)


## Test the Server with Jest

View the tests in the __test__ directory.

The configuration file can be found under the .jest folder.

Build the test image.
```bash
docker build -t stackup/hartree-gql-test -f Dockerfile.jest .
```

Run the test image.
```bash
 docker-compose up --abort-on-container-exit --exit-code-from graphql-engine

```

Note that swarm init may be required - this may require your IP address (replace IP address with your own IP address)
e.g. docker swarm init --advertise-addr 192.168.5.15
```bash
 docker swarm init

```
## Handy References!


- [Docker](https://www.docker.com/)
- [Docker compose](https://docs.docker.com/compose/)

